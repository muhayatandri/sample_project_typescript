const xmpp = require('simple-xmpp');


export class UnitedProductBot {
    private simpleXmpp: any;
    private jid: string;
    private password: string;
    private host: string;
    private port: number;
    private toJid: string;
    public ProductBot: UnitedProductBot;
    private validLogin: boolean;

    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }

    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }

    public Connect(jid: string, password: string, host: string, port = 5222): void {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
            });
            this.SetLogin(true)
        }

    }


    public statusConnection() {
        let self = this;
        try {
            xmpp.on('online', function (d: any) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin())
                } else {
                    console.log("STATUS LOGIN : ", self.IsLogin())
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        } catch (e) {
            this.validLogin = false;
            console.log(e)
        }

    }

    public SendMessage(transCode: string, toJid: string): void {
        xmpp.send("super_jet@jabberix.com", transCode + ".0300", false);
    }

    public GetMessage(): void {
        try {
            let self = this; // save object reference
            xmpp.on('chat', function (from: any, message: any) {
                if (message == null) {
                    console.log("no response")
                }
                // console.log("from other", message);
                self.UnitedRealtimePrice(message);
            });
        } catch (e) {
            this.validLogin = false;
            console.log("EXIT : ", e)
        }
    }

    UnitedRealtimePrice(msg: any) {
        // /Rp
        const regexPrice = /(SP\d*).{3}(Rp.\d+\.\d+)/gm;
        const strPrice = `HARGA PRODUK : TELKOMSEL PROMO 150000 | SP150 = Rp.146.000 | SP100 = Rp.96.625 | SP1 = Rp.1.300 | SP15 = Rp.14.775|SP10 = Rp.10.210 |`;
        let m;
        while ((m = regexPrice.exec(msg)) !== null) {
            if (m.index === regexPrice.lastIndex) {
                regexPrice.lastIndex++;
            }

            let tes = m.reduce((match: any, groupIndex: any, index: any) => {
                if (index === 1) {
                    match.push(groupIndex);
                }
                return match;
            }, []);
            console.log(tes)
        }
        const regexGangguan = /(SP\d*).{3}(Rp.\d+\.\d+).?(\[(.*?)\])/gm;
        const strGangguan = `HARGA PRODUK : TELKOMSEL PROMO 150000 | SP150 = Rp.146.000 | SP100 = Rp.96.625 | SP1 = Rp.1.300 [G] | SP15 = Rp.14.775|SP10 = Rp.10.210 |`;
        // let m;

        while ((m = regexGangguan.exec(msg)) !== null) {
            if (m.index === regexGangguan.lastIndex) {
                regexGangguan.lastIndex++;
            }

            m.forEach((match, groupIndex) => {
                console.log(`Found match, group ${groupIndex}: ${match}`);
            });
        }
    }

    public HandleUpdateDatabase(): void {

    }

    SetLogin(isLogin: boolean): void {
        this.validLogin = isLogin;
    }

    public IsLogin(): boolean {
        return this.validLogin;
    }

    public CloseConnection(): void {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }

    public ConnectionError(): void {
        let self = this;
        xmpp.on('error', function (err: any) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        })
    }
}
