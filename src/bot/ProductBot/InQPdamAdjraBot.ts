const xmpp = require('simple-xmpp');


export class InQPdamAdjraBot {
    private simpleXmpp: any;
    private jid: string;
    private password: string;
    private host: string;
    private port: number;
    private toJid: string;
    public ProductBot: InQPdamAdjraBot;
    private validLogin: boolean;

    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }

    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }

    public Connect(jid: string, password: string, host: string, port = 5222): void {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
            });
            this.SetLogin(true)
        }

    }


    public statusConnection() {
        let self = this;
        try {
            xmpp.on('online', function (d: any) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin())
                } else {
                    console.log("STATUS LOGIN : ", self.IsLogin())
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        } catch (e) {
            this.validLogin = false;
            console.log(e)
        }

    }

    public SendMessage(message: string, toJid: string): void {
        xmpp.send("super_jet@jabberix.com", message + ".0300", false);
    }

    public GetMessage(): void {
        xmpp.on('chat', function (from: any, message: any) {
            if (message == null) {
                console.log("no response")
            }
            console.log("from other", message);
        });
    }

    SetLogin(isLogin: boolean): void {
        this.validLogin = isLogin;
    }

    public IsLogin(): boolean {
        return this.validLogin;
    }

    public CloseConnection(): void {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }

    public ConnectionError(): void {
        let self = this;
        xmpp.on('error', function (err: any) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        })
    }
}
