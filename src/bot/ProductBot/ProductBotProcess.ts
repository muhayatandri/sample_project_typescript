const xmpp = require('simple-xmpp');


export class ProductBotProcess {
    private simpleXmpp: any;

    constructor() {
    }

    public Connect(): void {
        this.simpleXmpp = xmpp.connect({
            jid: 'qubli@jabbim.com',
            password: 'qubli00',
            host: 'jabbim.com',
            port: 5222,
            tls: true
        });
    }

    public SendMessage(): void {

        xmpp.send('ctr.01@unichat.in', 'CH.SP1', false);
    }

    public GetMessage(): void {
        xmpp.on('chat', function (from: any, message: any) {
            if (message == null) {
                console.log("no response")
            }
            console.log("from other", message);
        });

    }
}
