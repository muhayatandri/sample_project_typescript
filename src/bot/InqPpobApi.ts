import {TransactionBot} from "./TransactionBot/TransactionBot";
import {InqPpobArdraBot} from "./ProductBot/InqPpobArdraBot";
import {RequestApi} from "../dtos/Request";
import express from 'express'
import {Request, Response} from 'express'
import {createConnection} from "typeorm";
import {ProductsPpobRepository} from "../Repositories/ProductsPpobRepos/ProductsPpobRepository";
import {TagihanRepository} from "../Repositories/TagihanRepos/TagihanRepository";

interface IUserRequest extends express.Request {
    user: any
}


export class InqPpobApi {
    readonly inquiryBot: InqPpobArdraBot;
    readonly productPpobRepo: ProductsPpobRepository
    readonly tagihanRepo: TagihanRepository

    constructor() {
        this.tagihanRepo = new TagihanRepository()
        this.productPpobRepo = new ProductsPpobRepository()
        this.inquiryBot = new InqPpobArdraBot()
        this.inquiryBot.GetMessage()
    }


    public async DoProcess(req: Request, res: Response) {
        // await this.productPpobRepo.InitDb()
        this.inquiryBot.Connect("qubli@jabbim.com", "qubli00", "jabbim.com");
        this.inquiryBot.statusConnection()
        let message;
        let request: RequestApi = req.body
        console.log(req.body.nomor_rekening)
        let product = await this.productPpobRepo.FindProductBillDetailBy(request.product_detail_id)
        let counter = await this.productPpobRepo.GetCounter()
        this.inquiryBot.SetInQ(request.provider_ppob, request.nomor_rekening)
        let startDate = new Date()
        let endDate = new Date()
        startDate.setHours(7, 0o0, 0o0)
        endDate.setHours(30, 59, 59)
        if (product == null) {
            return res.status(424).json({"error": true, "status": 424, "message": "Error in database!"})
        }
        let current = await this.tagihanRepo.FindTagihanBy(request.nomor_rekening)
        if (current != null) {
            // console.log("start", startDate)
            // console.log("end", endDate)
            let currDate = new Date(current.created_at)
            currDate.setTime(currDate.getTime() - new Date().getTimezoneOffset() * 60 * 1000)
            console.log("cc", currDate.toISOString())
            if (currDate >= startDate && current.created_at <= currDate) {
                message = product.check_code + "." + request.nomor_rekening + ".0300R#" + counter.value
                await this.productPpobRepo.UpdateCounter({value: counter.value + 1}, counter.id)
                console.log("today")
            } else {
                await this.productPpobRepo.UpdateCounter({value: 2}, counter.id)
                message = product.check_code + "." + request.nomor_rekening + ".0300R#1"
                console.log("not today")
            }
        } else {
            message = product.check_code + "." + request.nomor_rekening + ".0300R#1"
            await this.tagihanRepo.CreateReqCheckTagihan({
                "phone_number": "",
                "nomor_rekening": request.nomor_rekening,
                "product_name": product.name,
                "status": "Proses check",
                // "user_id": null,
                "date_created": Date.now()
            })
        }
        console.log(message)
        // check whether customer request again to check bill or no,
        // message = "CEKAEJKT.30008997 + \".0300R#\" + counter"
        this.inquiryBot.SendMessage(message, "super_jet@jabberix.com", request.provider_ppob)
        return res.status(200).json("")

    }


    public db(): void {
        createConnection("development").then(async conn => {
            console.log("Database Connected is : ", conn.isConnected);
        });
    }
}
