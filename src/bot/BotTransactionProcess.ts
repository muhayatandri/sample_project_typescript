import {TransactionPpobRepository} from "../Repositories/TransactionPpobRepos/TransactionPpobRepository";
import {TransactionBot} from "./TransactionBot/TransactionBot";
import any = jasmine.any;
import {createConnection, getConnection} from "typeorm";
import {JabberSent} from "../models/JabberSent";
import {Jabbers} from "../models/Jabber";

export class BotTransactionProcess {
    readonly transRepos: TransactionPpobRepository;
    readonly transBot: TransactionBot;

    constructor() {
        this.transRepos = new TransactionPpobRepository();
        this.transBot = new TransactionBot();
        this.transBot.TransRepos = this.transRepos;
        // BotTransactionProcess.db();
        // this.transBot.GetMessage(); // comment for few
        // this.transBot.CloseConnection();
    }

    public async Connect() {
        await this.transRepos.InitDb();
    }

    public async DoProcess(): Promise<void> {
        console.log("ts")
        this.transBot.GetMessage(); // comment for few
        await this.transRepos.InitDb();
        let self = this;
        let jabber = await self.transRepos.GetJabberBy({id: ''});
        if (jabber.is_center_transaction != 0 && jabber.is_gangguan == 0) {
            let jdid: String = jabber.jabber;
            this.transBot.Connect(jdid, "qubli00", "jabbim.com");
        }
        this.transBot.statusConnection();
        try {
            let data: any = ["MK5", "MK10", "MK50"];
            let o = 0;
            let interval = setInterval(async function () {
                // if (o === 50) {
                //     clearInterval(interval); //Call the method when you want to clear it.
                // }

                // for (let i of data) {
                //     self.transBot.SendMessage(i + ".08521115032" + o, "super_jet@jabberix.com")
                // }
                // console.log(o);
                o++;
                if (self.transBot.IsLogin()) {
                    let datas: JabberSent[] = await self.transRepos.GetJabberMessage({jabber_id: ''});
                    if (datas.length > 0) {
                        for (let i of datas) {
                            //get ref id
                            let msg: String = i.messages;
                            let toJid: String = i.jabber.jabber;
                            self.transBot.SendMessage(msg, toJid, i.jabber_id);
                            await self.transRepos.UpdateById({is_sent: 1}, i.jabber_sent_id)
                        }
                    }
                } else {
                    console.log("Err! Bot must to login")
                }
            }, 2000);
        } catch (e) {
            console.log("Err!bot must be restart.");
            this.transBot.CloseConnection();
            this.transBot.ConnectionError();
        }
    }

    public async ResDoProcess(req: Request, res: Response): Promise<void> {
        await this.transRepos.InitDb();
        let self = this;
        let jabber = await self.transRepos.GetJabberBy({id: ''});
        if (jabber.is_center_transaction != 0 && jabber.is_gangguan == 0) {
            let jdid: String = jabber.jabber;
            this.transBot.Connect(jdid, "qubli00", "jabbim.com");
        }
        this.transBot.statusConnection();
        try {
            let data: any = ["MK5", "MK10", "MK50"];
            let o = 0;
            let interval = setInterval(async function () {
                // if (o === 50) {
                //     clearInterval(interval); //Call the method when you want to clear it.
                // }

                // for (let i of data) {
                //     self.transBot.SendMessage(i + ".08521115032" + o, "super_jet@jabberix.com")
                // }
                console.log(o);
                o++;
                if (self.transBot.IsLogin()) {
                    let datas: JabberSent[] = await self.transRepos.GetJabberMessage({jabber_id: ''});
                    if (datas.length > 0) {
                        for (let i of datas) {
                            let msg: String = i.messages;
                            let toJid: String = i.jabber.jabber;
                            self.transBot.SendMessage(msg, toJid, i.jabber_id);
                            await self.transRepos.UpdateById({is_sent: 1}, i.jabber_sent_id)
                        }
                    }
                } else {
                    console.log("Err! Bot must to login")
                }
            }, 2000);

        } catch (e) {
            console.log("Err!bot must be restart.");
            this.transBot.CloseConnection();
            this.transBot.ConnectionError();
        }
    }

    public async RestartBot(req: Request, res: Response): Promise<void> {

    }

    public db(): void {
        createConnection("development").then(async conn => {
            console.log("Database Connected is : ", conn.isConnected);
        });
    }
}

// let b = new BotTransactionProcess();
// b.DoProcess();

// b.db();
// b.Connect().then(function (con: any) {
//     // console.log(con)
// });

