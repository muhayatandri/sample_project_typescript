import {TransactionPpobRepository} from "../../Repositories/TransactionPpobRepos/TransactionPpobRepository";
import {createConnection} from "typeorm";

const {v4: uuidv4} = require('uuid');
const xmpp = require('simple-xmpp');


export class TransactionBot {
    private simpleXmpp: any;
    private jid: string;
    private password: string;
    private host: string;
    private port: number;
    private toJid: string;
    private callbackUrl: string;
    private jabber_id = 0
    public TransBot: TransactionBot;
    private validLogin: boolean;
    public TransRepos: TransactionPpobRepository;

    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }


    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }

    public Connect(jid: String, password: String, host: String, port = 5222): void {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
                // jid: 'qubli@jabbim.com',
                // password: 'qubli00',
                // host: 'jabbim.com',
                // port: 5222,
                // tls: true
            });
            this.SetLogin(true)
        }

    }


    public statusConnection() {
        // console.log("status login : " + this.validLogin);
        let self = this;
        try {
            xmpp.on('online', function (d: any) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin())
                } else {
                    console.log("STATUS LOGIN : ", self.IsLogin())
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        } catch (e) {
            self.SetLogin(false);
            console.log(e)
        }

    }

    public SendMessage(message: String, toJid: String, jabber_id: number): void {
        this.jabber_id = jabber_id
        xmpp.send(toJid, message, false);
    }

    public GetMessage(): void {
        let self = this;
        try {
            xmpp.on('chat', function (from: any, message: any) {
                if (message == null) {
                    console.log("no response")
                }
                // console.log("from other", from);
                self.HandleMessage(message, from)
            });
        } catch (e) {
            console.log("something error");
            self.SetLogin(false);
        }

    }

    public HandleMessage(message: any, fromJid: any): void {
        try {
            let obj = {
                // uuid: uuidv4(),
                message_responses: message,
                note: "callback jabber response.",
                is_process: 0,
                jabber: fromJid,
                jabber_id: this.jabber_id,
                date_created: new Date().toISOString()
            };
            let resp = this.TransRepos.SaveParsingProduct(obj);
            if (resp != null || resp != undefined) {
                console.log("Success data insert to parsing product.")
            } else {
                console.log("something error")
            }
        } catch (e) {
            console.log(e);
            console.log("something error")
        }
    }

    public Callback(msg: any) {

    }

    SetLogin(isLogin: boolean): void {
        this.validLogin = isLogin;
    }

    public IsLogin(): boolean {
        return this.validLogin;
    }

    public CloseConnection(): void {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }

    public ConnectionError(): void {
        let self = this;
        xmpp.on('error', function (err: any) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        })
    }
}

