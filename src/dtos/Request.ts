export interface RequestApi {
    provider_ppob: string,
    nomor_rekening: string,
    product_detail_id: number,
    user_id : string
}
