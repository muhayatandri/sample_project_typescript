import {JabberSent} from "../models/JabberSent";
import {ObjectLiteral} from "typeorm";

export class JabberDto {


    public static ToDtoJabberResult(datas: JabberSent[]): any { //DATA_TRANSFER_OBJECT
        if (datas != null) {
            console.log(datas)
        }

    }

}

export interface JabberResults {
    id: Number;
    jabber: String;
    messages: String;
    is_active: Number
    is_sent: Number
}
