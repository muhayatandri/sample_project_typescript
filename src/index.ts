import "reflect-metadata";
const xmpp = require('simple-xmpp');
// Connect ke server XMPP
xmpp.connect({
    jid: 'qubli@jabbim.com',
    password: 'qubli00',
    host: 'jabbim.com',
    port: 5222,
    tls: true

});
// Kirim pesan ke penerima
xmpp.send('ctr.01@unichat.in', 'CH.SP1', false);

// Jika xmpp client kita berhasil terhubung / login ke xmpp server, maka tampilkan pesan berikut
xmpp.on('online', function (data : any) {
    console.log('Terkoneksi dengan Jabber ID : ' + data.jid);
    console.log('Terkirim ke Jabber.');
    console.log('Menunggu reply.');
});

// // Jika ada error di xmpp, munculkan pesannya
// xmpp.on('error', function (err) {
//     console.error(err);
// });

// Jika ada yang melakukan permintaan pertemanan, kita accept secara otomatis
// xmpp.on('subscribe', function (from) {
//     console.log(from + ' ingin menambahkan anda sebagai teman')
//     xmpp.acceptSubscription(from);
//     console.log(from + ' telah ditambahkan sebagai teman')
// })
// Tampilkan pesan jika ada pesan masuk (terima pesan)
xmpp.on('chat', function (from : any, message : any) {
    if (message == null) {
        console.log("no response")
    }
    console.log("from others", message);
});
