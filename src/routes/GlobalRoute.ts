// import {UserRoute} from "./ApiRoutes/UserRoute";
import {Router} from "express";
import {BotTransactionProcess} from "../bot/BotTransactionProcess";
import {InqPpobApi} from "../bot/InqPpobApi";
// import CustomerRoute from "./ApiRoutes/CustomerRoute";
require('express-group-routes');

export class GlobalRoute {
    // private userRoute: UserRoute = new UserRoute();
    private botTransProcess: BotTransactionProcess = new BotTransactionProcess();
    private inqPpobAapi: InqPpobApi = new InqPpobApi();

    private readonly route: Router;

    constructor(app: any) {
        this.route = app;
        app.route("/tes").post(function (req: Request, res: Response) {
            console.log("Tes")
        })
        app.route("/check-tagihan").post(this.inqPpobAapi.DoProcess.bind(this.inqPpobAapi));

        app.group("/bot/v1", (router: any) => {
            app.route("/jabber-process").post(this.botTransProcess.ResDoProcess.bind(this.botTransProcess));
            app.route("/restart-bot").post(this.botTransProcess.RestartBot.bind(this.botTransProcess));
            // this.userRoute.route(router);
            // this.customerRoute.route(router);
        });
    }

    public routes(app: any): void {
        // this.userRoute.route(app)
    }

    public getRoutes(): any {
        return this.route;
    }
}
