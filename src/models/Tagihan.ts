import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {ProductBillDetailPpob} from "./ProductBillDetailPpob";


@Entity({name: "tagihans"})
export class Tagihan {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    phone_number: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    user_id: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    nomor_rekening: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    product_name: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    status: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    messages: String;


    @Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    })
    date_created: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    created_at: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    updated_at: Date;
}

module.exports = {
    Tagihan: Tagihan
};
