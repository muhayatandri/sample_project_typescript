import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Jabbers} from "./Jabber";

@Entity({name: "jabber_sents"})
export class JabberSent {
    @PrimaryGeneratedColumn()
    jabber_sent_id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_sent: any;
    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    trans_ppob_id: any;

    // @Column({
    //     // length: 50,
    //     nullable: true,
    //     type: "character varying"
    // })
    // to_jid: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    types: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    messages: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"

    })
    jabber_id: any;

    // @Column({
    //     // length: 50,
    //     nullable: true,
    //     type: "integer"
    //
    // })
    // status_id: any;

    @Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    })
    date_created: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    created_at: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    updated_at: Date;


    @OneToOne(type => Jabbers, jabber => jabber.jabberSent, {
        nullable: true,
        // primary : true,
    })
    @JoinColumn({name: "jabber_id", referencedColumnName: "jabber_id"})
    jabber: Jabbers;

    // @OneToOne(type => Jabbers)
    // @JoinColumn()
    // jabber: Jabbers;

}

module.exports = {
    JabberSent: JabberSent
};
