import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity({name: "product_bill_ppob_details"})
export class ProductBillDetailPpob {
    @PrimaryGeneratedColumn()
    productbilldetailppob_id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    productbillppob_id: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    product_code: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    optional_code: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    check_code: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_active: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    slug: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    name: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    description: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "decimal"
    })
    admin_fee: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "decimal"
    })
    fee: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "decimal"
    })
    sell_price: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "decimal"
    })
    agent_price: any;

    @Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    })
    date_created: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    created_at: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    updated_at: Date;
}

module.exports = {
    ProductBillDetailPpob: ProductBillDetailPpob
};

