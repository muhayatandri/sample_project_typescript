import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {JabberSent} from "./JabberSent";

@Entity({name: "parsing_products"})
export class ParsingProduct {
    @PrimaryGeneratedColumn()
    parsing_product_id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    jabber: String;

    // @Column({
    //     // length: 50,
    //     nullable: true,
    //     type: "character varying"
    // })
    // uuid: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    destination_phone_number: String;


    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    message_responses: String;
    //
    // @Column({
    //     // length: 50,
    //     nullable: true,
    //     type: "integer"
    // })
    // is_sent: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_process: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    jabber_id: any;


    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    ref_id: any;


    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    note: String;


    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    msidn: String;


    @Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    })
    date_created: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    created_at: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    updated_at: Date;
}

module.exports = {
    ParsingProduct: ParsingProduct
};
