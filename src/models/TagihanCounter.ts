import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Tagihan} from "./Tagihan";

@Entity({name: "tagihan_counters"})
export class TagihanCounter {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    counter_tg: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    counter_tk: any;


    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    value: any;


    @Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    })
    date_created: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    created_at: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    updated_at: Date;
}

module.exports = {
    TagihanCounter: TagihanCounter
};
