import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity({name: "product_ppobs"})
export class Tagihan {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    jabber: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    uuid: String;



}
