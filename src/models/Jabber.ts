import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {JabberSent} from "./JabberSent";

@Entity({name: "jabbers"})
export class Jabbers {
    @PrimaryGeneratedColumn()
    jabber_id: number;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    provider_id: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    })
    jabber: String;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_active: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_gangguan: any;

    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_center_transaction: any;


    @Column({
        // length: 50,
        nullable: true,
        type: "integer"
    })
    is_used: any;

    // @Column({
    //     // length: 50,
    //     nullable: true,
    //     type: "integer"
    // })
    // is_used_transaction: any;
    @Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    })
    date_created: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    created_at: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP',})
    updated_at: Date;

    @OneToOne(type => JabberSent, jabberSent => jabberSent.jabber, {
        nullable: true,
        primary: true
        // cascade : no
    })
    @JoinColumn({name: "jabber_id", referencedColumnName: "jabber_id"})
    jabberSent: JabberSent;
}

module.exports = {
    Jabbers: Jabbers
};
