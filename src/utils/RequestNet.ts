export class RequestNet {
    private data: any;
    private url: any;


    constructor(data: any, url: any) {
        this.data = data;
        this.url = url;
    }


    async get(): Promise<any> {
        const res = await fetch(this.url);
        const data = res.json();
        return new ParsedResponse("", data).response // passed the response to another class
    }

    async post() {

    }

    async put() {

    }

    async delete() {

    }
}

export class ParsedResponse {
    private readonly _response: any;
    url: any;


    constructor(url: any, result: any,) {
        this._response = result;
        this.url = url;
    }


    get response(): any {
        return this._response;
    }
}
