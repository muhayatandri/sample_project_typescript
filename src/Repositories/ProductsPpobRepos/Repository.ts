import {ProductBillDetailPpob} from "../../models/ProductBillDetailPpob";
import {TagihanCounter} from "../../models/TagihanCounter";

export interface Repository {
    FindProductBillDetailBy(obj: any): Promise<ProductBillDetailPpob>

    GetCounter(): Promise<TagihanCounter>

    UpdateCounter(data: any, id : number): Promise<void>

}
