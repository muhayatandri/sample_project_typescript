import {Repository} from "./Repository";
import {ProductBillDetailPpob} from "../../models/ProductBillDetailPpob";
import {Connection, createConnection, getConnection} from "typeorm";
import {JabberSent} from "../../models/JabberSent";
import {TagihanCounter} from "../../models/TagihanCounter";


export class ProductsPpobRepository implements Repository {

    private connection: Connection;
    private Tes: string

    public async InitDb() {
        this.connection = await createConnection("development");
    }


    private static async getConnectionProductBillDetail(): Promise<any> {
        return getConnection("development").getRepository(ProductBillDetailPpob);
        // return this.connection.getRepository(ProductBillDetailPpob);

    }

    private static async getConnectionTagihanCounter(): Promise<any> {
        return getConnection("development").getRepository(TagihanCounter);
        // return this.connection.getRepository(TagihanCounter);

    }


    async FindProductBillDetailBy(productDetailId: number): Promise<ProductBillDetailPpob> {
        const model = await ProductsPpobRepository.getConnectionProductBillDetail()
        return await model.createQueryBuilder("productBillDetail")
            .where("productBillDetail.productbilldetailppob_id = :productbilldetailppob_id",
                {productbilldetailppob_id: productDetailId}).getOne()
    }

    async GetCounter(): Promise<TagihanCounter> {
        const model = await ProductsPpobRepository.getConnectionTagihanCounter()
        return model.createQueryBuilder("counter")
            .getOne()

    }

    async UpdateCounter(data: any, id: number): Promise<void> {
        const model = await ProductsPpobRepository.getConnectionTagihanCounter()
        return await model.createQueryBuilder()
            .update()
            .set(data)
            .where("id = :id", {id: id})
            // .update({...obj})
            // .returning('*')
            .execute();
    }


}
