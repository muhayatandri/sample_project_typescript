import {JabberSent} from "../../models/JabberSent";
import {Jabbers} from "../../models/Jabber";

export interface Repository {
    GetTransactionBy(obj: any): Promise<any>

    GetJabberBy(obj: any): Promise<Jabbers>

    GetJabberMessage(obj: any): Promise<JabberSent[]>

    UpdateById(obj: any, id: number | undefined): Promise<any>

    SaveParsingProduct(obj: any): Promise<any>


}
