import {Brackets, Connection, createConnection, getConnection, getRepository} from "typeorm";
import {TransactionPpobs} from "../../models/TransactionPpobs";
import {Repository} from "./Repository";
import {JabberSent} from "../../models/JabberSent";
import {JabberDto} from "../../dtos/JabberDto";
import {Jabbers} from "../../models/Jabber";
import {ParsingProduct} from "../../models/ParsingProducts";

export class TransactionPpobRepository implements Repository {


    private connection: Connection;

    public async InitDb() {
        this.connection = await createConnection("development");
    }

    constructor() {
    }

    private async getConnectionJabberSent(): Promise<any> {
        return this.connection.getRepository(JabberSent);

    }

    private async getConnectionJabber(): Promise<any> {
        return this.connection.getRepository(Jabbers);

    }

    private async getConnectionParsingProduct(): Promise<any> {
        return this.connection.getRepository(ParsingProduct);

    }

    async GetTransactionBy(obj: any): Promise<any> {
        // const model = this.getConnectionUser();
        // return await model.find();
    }

    async GetJabberBy(obj: any): Promise<Jabbers> {
        const model = await this.getConnectionJabber();
        let datas = model.createQueryBuilder("jabber")
            .where("jabber.is_active = :is_active", {is_active: 1});
        // .andWhere(new Brackets((qb) => {
        //     qb.where("jabber.provider_id = :provider_id", {id: 0});
        // }));
        if (obj.id != '') {
            datas = datas.where("jabber.jabber_id = :jabber_id", {jabber_id: obj.id})
        } else {
            datas = datas.where("jabber.jabber_id = :jabber_id", {jabber_id: 8})
        }
        return await datas.getOne()
    }

    async GetJabberMessage(obj: any): Promise<JabberSent[]> {
        const model = await this.getConnectionJabberSent();
        let datas = model.createQueryBuilder("jabberSent")
            .innerJoinAndSelect("jabberSent.jabber", "jabber")
            .andWhere(new Brackets((qb => {
                qb.where("jabber.is_used = :is_used", {is_used: 1})
                    .andWhere("jabber.provider_id != :provider_id", {provider_id: 0})
                // .andWhere("jabber.is_used_transaction = :is_used_transaction", {is_used_transaction: 1})
            })))
            .andWhere(new Brackets((qb => {
                qb.where("jabber.is_gangguan = :is_gangguan", {is_gangguan: 0})
                    .andWhere("jabber.is_active = :is_active", {is_active: 1})
            })))
            .where("jabberSent.is_sent = :is_sent", {is_sent: 0})
        // .andWhere(new Brackets((qb) => {
        //     qb.where("jabberSent.status_id = :status_id",{status_id : 8})
        //         .orWhere("")
        // }))
        // .getMany();
        if (obj.jabber_id !== '') {
            datas = datas.where("jabberSent.jabber_id = :jabber_id", {jabber_id: obj.jabber_id})
        }
        return await datas.getMany();
    }

    async UpdateById(obj: any, id: number | undefined): Promise<any> {
        const model = await this.getConnectionJabberSent();
        return await model.createQueryBuilder()
            .update()
            .set(obj)
            .where("jabber_sent_id = :jabber_sent_id", {jabber_sent_id: id})
            // .update({...obj})
            // .returning('*')
            .execute();
    }

    async SaveParsingProduct(obj: any): Promise<any> {
        const model = await this.getConnectionParsingProduct();
        return await model.createQueryBuilder()
            .insert()
            .values({...obj})
            .returning('*')
            .execute();
    }

}
