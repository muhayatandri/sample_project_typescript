import {Tagihan} from "../../models/Tagihan";

export interface Repository {
    CreateReqCheckTagihan(obj: any): Promise<any>
    FindTagihanBy(nomorRekening : string) : Promise<Tagihan>
}
