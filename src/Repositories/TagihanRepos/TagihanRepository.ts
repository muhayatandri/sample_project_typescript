import {Repository} from "./Repository";
import {Connection, createConnection, getConnection} from "typeorm";
import {Tagihan} from "../../models/Tagihan";
import {ProductBillDetailPpob} from "../../models/ProductBillDetailPpob";


export class TagihanRepository implements Repository {


    private connection: Connection;


    public async InitDb() {
        this.connection = await createConnection("development");
    }


    private async getConnectionTagihan(): Promise<any> {
        return getConnection("development").getRepository(Tagihan);
        // return this.connection.getRepository(Tagihan);
    }


    async CreateReqCheckTagihan(obj: any): Promise<any> {
        const model = await this.getConnectionTagihan();
        return await model.createQueryBuilder()
            .insert()
            .values({...obj})
            .returning('*')
            .execute();
    }

    async FindTagihanBy(nomorRekening: string): Promise<Tagihan> {
        let todayBefore = ""
        const model = await this.getConnectionTagihan()
        return model.createQueryBuilder("tagihan")
            .where("tagihan.nomor_rekening = :nomor_rekening", {nomor_rekening: nomorRekening})
            .orderBy("tagihan.id", "DESC")
            .getOne()
    }

}
