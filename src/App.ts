import express from "express";

require('express-group-routes');
import * as bodyParser from "body-parser";
import {GlobalRoute} from "./routes/GlobalRoute";
import compression from "compression";  // compresses requests
import cors from "cors";
import {createConnection, getConnection} from "typeorm";
import morgan from 'morgan';
import passport from "passport";
import {TransactionBot} from "./bot/TransactionBot/TransactionBot";
import {BotTransactionProcess} from "./bot/BotTransactionProcess";
import {JabberSent} from "./models/JabberSent";
import {ProductsPpobRepository} from "./Repositories/ProductsPpobRepos/ProductsPpobRepository";
import {ProductBillDetailPpob} from "./models/ProductBillDetailPpob";
import {Jabbers} from "./models/Jabber";
import {Tagihan} from "./models/Tagihan";
// import PassportAuth from "./security/PassportAuth";
require('express-group-routes');
var timeout = require('express-timeout-handler');


const {exec} = require('child_process');
const CronJob = require('cron').CronJob;
const restartCommand = "pm2 restart 0";
const listCommand = "pm2 list";

class App {
    public app: express.Application;
    public routePrev: GlobalRoute;

    constructor() {
        // let ts = new TransactionBot();
        // ts.Connect();
        // ts.SendMessage();
        // ts.GetMessage();
        this.app = express();
        this.config();
        App.db();
        this.routePrev = new GlobalRoute(this.app);
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(compression());
        this.app.use(cors({credentials: true, origin: true}));
        this.app.use(morgan('dev'));
        this.app.use(passport.initialize());
        this.app.use(passport.session());
        // new PassportAuth(this.app,passport);
    }

    private static async db(): Promise<void> {
        console.log(new Date("2020-05-31 10:39:26.329000 +00:00").toISOString())
        let startDate = new Date()
        let endDate = new Date()
        startDate.setHours(7, 0o1, 0o0)
        endDate.setHours(30, 59, 59)
        // startDate.setHours(startDate.setHours(21,10,10))
        console.log("end",endDate)
        const filters = {
            before: startDate.toISOString() as any,
            after: endDate.toISOString() as any,
            // before,
            // after
        };

        await createConnection('development').then(async conn => {
            console.log("Database Connected is : ", conn.isConnected)
        });
        // let nm = "1234"
        // const model = getConnection("development").getRepository(Tagihan);
        // let tes = await model.createQueryBuilder("tagihan")
        //     .where("tagihan.nomor_rekening = :nomor_rekening", {nomor_rekening : "123456"})
        //     .orderBy("tagihan.id", "DESC")
        //     .getOne()
        // // @ts-ignore
        // if (tes !=null){
        //     console.log(startDate)
        //     console.log(endDate)
        //     if (tes.created_at >= startDate && tes.created_at <= endDate) {
        //         console.log("today")
        //     } else {
        //         console.log("not today")
        //     }
        // }else {
        //     console.log("cret")
        // }


        // console.log(tes)
        // const model = getConnection("development").getRepository(Jabbers);
        // let data: any[] = await model.find({where: {jabber_id: 1}});
        // console.log(data)
        // let b = new BotTransactionProcess();
        // let promise = b.Connect();
    }
}

export default new App().app
