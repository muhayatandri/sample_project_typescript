"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionPpobRepository = void 0;
const typeorm_1 = require("typeorm");
const JabberSent_1 = require("../../models/JabberSent");
const Jabber_1 = require("../../models/Jabber");
const ParsingProducts_1 = require("../../models/ParsingProducts");
class TransactionPpobRepository {
    constructor() {
    }
    InitDb() {
        return __awaiter(this, void 0, void 0, function* () {
            this.connection = yield typeorm_1.createConnection("development");
        });
    }
    getConnectionJabberSent() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.connection.getRepository(JabberSent_1.JabberSent);
        });
    }
    getConnectionJabber() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.connection.getRepository(Jabber_1.Jabbers);
        });
    }
    getConnectionParsingProduct() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.connection.getRepository(ParsingProducts_1.ParsingProduct);
        });
    }
    GetTransactionBy(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            // const model = this.getConnectionUser();
            // return await model.find();
        });
    }
    GetJabberBy(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield this.getConnectionJabber();
            let datas = model.createQueryBuilder("jabber")
                .where("jabber.is_active = :is_active", { is_active: 1 });
            // .andWhere(new Brackets((qb) => {
            //     qb.where("jabber.provider_id = :provider_id", {id: 0});
            // }));
            if (obj.id != '') {
                datas = datas.where("jabber.jabber_id = :jabber_id", { jabber_id: obj.id });
            }
            else {
                datas = datas.where("jabber.jabber_id = :jabber_id", { jabber_id: 8 });
            }
            return yield datas.getOne();
        });
    }
    GetJabberMessage(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield this.getConnectionJabberSent();
            let datas = model.createQueryBuilder("jabberSent")
                .innerJoinAndSelect("jabberSent.jabber", "jabber")
                .andWhere(new typeorm_1.Brackets((qb => {
                qb.where("jabber.is_used = :is_used", { is_used: 1 })
                    .andWhere("jabber.provider_id != :provider_id", { provider_id: 0 });
                // .andWhere("jabber.is_used_transaction = :is_used_transaction", {is_used_transaction: 1})
            })))
                .andWhere(new typeorm_1.Brackets((qb => {
                qb.where("jabber.is_gangguan = :is_gangguan", { is_gangguan: 0 })
                    .andWhere("jabber.is_active = :is_active", { is_active: 1 });
            })))
                .where("jabberSent.is_sent = :is_sent", { is_sent: 0 });
            // .getMany();
            if (obj.jabber_id !== '') {
                datas = datas.where("jabberSent.jabber_id = :jabber_id", { jabber_id: obj.jabber_id });
            }
            return yield datas.getMany();
        });
    }
    UpdateById(obj, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield this.getConnectionJabberSent();
            return yield model.createQueryBuilder()
                .update()
                .set(obj)
                .where("jabber_sent_id = :jabber_sent_id", { jabber_sent_id: id })
                // .update({...obj})
                // .returning('*')
                .execute();
        });
    }
    SaveParsingProduct(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield this.getConnectionParsingProduct();
            return yield model.createQueryBuilder()
                .insert()
                .values(Object.assign({}, obj))
                .returning('*')
                .execute();
        });
    }
}
exports.TransactionPpobRepository = TransactionPpobRepository;
//# sourceMappingURL=TransactionPpobRepository.js.map