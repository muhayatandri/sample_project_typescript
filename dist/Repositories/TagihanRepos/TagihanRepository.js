"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TagihanRepository = void 0;
const typeorm_1 = require("typeorm");
const Tagihan_1 = require("../../models/Tagihan");
class TagihanRepository {
    InitDb() {
        return __awaiter(this, void 0, void 0, function* () {
            this.connection = yield typeorm_1.createConnection("development");
        });
    }
    getConnectionTagihan() {
        return __awaiter(this, void 0, void 0, function* () {
            return typeorm_1.getConnection("development").getRepository(Tagihan_1.Tagihan);
            // return this.connection.getRepository(Tagihan);
        });
    }
    CreateReqCheckTagihan(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield this.getConnectionTagihan();
            return yield model.createQueryBuilder()
                .insert()
                .values(Object.assign({}, obj))
                .returning('*')
                .execute();
        });
    }
    FindTagihanBy(nomorRekening) {
        return __awaiter(this, void 0, void 0, function* () {
            let todayBefore = "";
            const model = yield this.getConnectionTagihan();
            return model.createQueryBuilder("tagihan")
                .where("tagihan.nomor_rekening = :nomor_rekening", { nomor_rekening: nomorRekening })
                .orderBy("tagihan.id", "DESC")
                .getOne();
        });
    }
}
exports.TagihanRepository = TagihanRepository;
//# sourceMappingURL=TagihanRepository.js.map