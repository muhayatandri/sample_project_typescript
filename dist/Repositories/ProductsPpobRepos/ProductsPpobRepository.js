"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsPpobRepository = void 0;
const ProductBillDetailPpob_1 = require("../../models/ProductBillDetailPpob");
const typeorm_1 = require("typeorm");
const TagihanCounter_1 = require("../../models/TagihanCounter");
class ProductsPpobRepository {
    InitDb() {
        return __awaiter(this, void 0, void 0, function* () {
            this.connection = yield typeorm_1.createConnection("development");
        });
    }
    static getConnectionProductBillDetail() {
        return __awaiter(this, void 0, void 0, function* () {
            return typeorm_1.getConnection("development").getRepository(ProductBillDetailPpob_1.ProductBillDetailPpob);
            // return this.connection.getRepository(ProductBillDetailPpob);
        });
    }
    static getConnectionTagihanCounter() {
        return __awaiter(this, void 0, void 0, function* () {
            return typeorm_1.getConnection("development").getRepository(TagihanCounter_1.TagihanCounter);
            // return this.connection.getRepository(TagihanCounter);
        });
    }
    FindProductBillDetailBy(productDetailId) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield ProductsPpobRepository.getConnectionProductBillDetail();
            return yield model.createQueryBuilder("productBillDetail")
                .where("productBillDetail.productbilldetailppob_id = :productbilldetailppob_id", { productbilldetailppob_id: productDetailId }).getOne();
        });
    }
    GetCounter() {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield ProductsPpobRepository.getConnectionTagihanCounter();
            return model.createQueryBuilder("counter")
                .getOne();
        });
    }
    UpdateCounter(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield ProductsPpobRepository.getConnectionTagihanCounter();
            return yield model.createQueryBuilder()
                .update()
                .set(data)
                .where("id = :id", { id: id })
                // .update({...obj})
                // .returning('*')
                .execute();
        });
    }
}
exports.ProductsPpobRepository = ProductsPpobRepository;
//# sourceMappingURL=ProductsPpobRepository.js.map