"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GlobalRoute = void 0;
const BotTransactionProcess_1 = require("../bot/BotTransactionProcess");
const InqPpobApi_1 = require("../bot/InqPpobApi");
// import CustomerRoute from "./ApiRoutes/CustomerRoute";
require('express-group-routes');
class GlobalRoute {
    constructor(app) {
        // private userRoute: UserRoute = new UserRoute();
        this.botTransProcess = new BotTransactionProcess_1.BotTransactionProcess();
        this.inqPpobAapi = new InqPpobApi_1.InqPpobApi();
        this.route = app;
        app.route("/tes").post(function (req, res) {
            console.log("Tes");
        });
        app.route("/check-tagihan").post(this.inqPpobAapi.DoProcess.bind(this.inqPpobAapi));
        app.group("/bot/v1", (router) => {
            app.route("/jabber-process").post(this.botTransProcess.ResDoProcess.bind(this.botTransProcess));
            app.route("/restart-bot").post(this.botTransProcess.RestartBot.bind(this.botTransProcess));
            // this.userRoute.route(router);
            // this.customerRoute.route(router);
        });
    }
    routes(app) {
        // this.userRoute.route(app)
    }
    getRoutes() {
        return this.route;
    }
}
exports.GlobalRoute = GlobalRoute;
//# sourceMappingURL=GlobalRoute.js.map