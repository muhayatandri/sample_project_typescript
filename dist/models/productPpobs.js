"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tagihan = void 0;
const typeorm_1 = require("typeorm");
let Tagihan = /** @class */ (() => {
    let Tagihan = class Tagihan {
    };
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Tagihan.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], Tagihan.prototype, "jabber", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], Tagihan.prototype, "uuid", void 0);
    Tagihan = __decorate([
        typeorm_1.Entity({ name: "product_ppobs" })
    ], Tagihan);
    return Tagihan;
})();
exports.Tagihan = Tagihan;
//# sourceMappingURL=ProductPpobs.js.map