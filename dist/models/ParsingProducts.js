"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParsingProduct = void 0;
const typeorm_1 = require("typeorm");
let ParsingProduct = /** @class */ (() => {
    let ParsingProduct = class ParsingProduct {
    };
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], ParsingProduct.prototype, "parsing_product_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ParsingProduct.prototype, "jabber", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ParsingProduct.prototype, "destination_phone_number", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ParsingProduct.prototype, "message_responses", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], ParsingProduct.prototype, "is_process", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], ParsingProduct.prototype, "jabber_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], ParsingProduct.prototype, "ref_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ParsingProduct.prototype, "note", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ParsingProduct.prototype, "msidn", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 255,
            nullable: true,
            type: "timestamp"
        }),
        __metadata("design:type", Date)
    ], ParsingProduct.prototype, "date_created", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], ParsingProduct.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], ParsingProduct.prototype, "updated_at", void 0);
    ParsingProduct = __decorate([
        typeorm_1.Entity({ name: "parsing_products" })
    ], ParsingProduct);
    return ParsingProduct;
})();
exports.ParsingProduct = ParsingProduct;
module.exports = {
    ParsingProduct: ParsingProduct
};
//# sourceMappingURL=ParsingProducts.js.map