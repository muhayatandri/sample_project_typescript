"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Jabbers = void 0;
const typeorm_1 = require("typeorm");
const JabberSent_1 = require("./JabberSent");
let Jabbers = /** @class */ (() => {
    let Jabbers = class Jabbers {
    };
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Jabbers.prototype, "jabber_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], Jabbers.prototype, "provider_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], Jabbers.prototype, "jabber", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], Jabbers.prototype, "is_active", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], Jabbers.prototype, "is_gangguan", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], Jabbers.prototype, "is_center_transaction", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], Jabbers.prototype, "is_used", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 255,
            nullable: true,
            type: "timestamp"
        }),
        __metadata("design:type", Date)
    ], Jabbers.prototype, "date_created", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], Jabbers.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], Jabbers.prototype, "updated_at", void 0);
    __decorate([
        typeorm_1.OneToOne(type => JabberSent_1.JabberSent, jabberSent => jabberSent.jabber, {
            nullable: true,
            primary: true
            // cascade : no
        }),
        typeorm_1.JoinColumn({ name: "jabber_id", referencedColumnName: "jabber_id" }),
        __metadata("design:type", JabberSent_1.JabberSent)
    ], Jabbers.prototype, "jabberSent", void 0);
    Jabbers = __decorate([
        typeorm_1.Entity({ name: "jabbers" })
    ], Jabbers);
    return Jabbers;
})();
exports.Jabbers = Jabbers;
module.exports = {
    Jabbers: Jabbers
};
//# sourceMappingURL=Jabber.js.map