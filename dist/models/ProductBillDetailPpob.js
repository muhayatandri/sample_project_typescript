"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductBillDetailPpob = void 0;
const typeorm_1 = require("typeorm");
let ProductBillDetailPpob = /** @class */ (() => {
    let ProductBillDetailPpob = class ProductBillDetailPpob {
    };
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], ProductBillDetailPpob.prototype, "productbilldetailppob_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], ProductBillDetailPpob.prototype, "productbillppob_id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ProductBillDetailPpob.prototype, "product_code", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ProductBillDetailPpob.prototype, "optional_code", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ProductBillDetailPpob.prototype, "check_code", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], ProductBillDetailPpob.prototype, "is_active", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ProductBillDetailPpob.prototype, "slug", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ProductBillDetailPpob.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "character varying"
        }),
        __metadata("design:type", String)
    ], ProductBillDetailPpob.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "decimal"
        }),
        __metadata("design:type", Object)
    ], ProductBillDetailPpob.prototype, "admin_fee", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "decimal"
        }),
        __metadata("design:type", Object)
    ], ProductBillDetailPpob.prototype, "fee", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "decimal"
        }),
        __metadata("design:type", Object)
    ], ProductBillDetailPpob.prototype, "sell_price", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "decimal"
        }),
        __metadata("design:type", Object)
    ], ProductBillDetailPpob.prototype, "agent_price", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 255,
            nullable: true,
            type: "timestamp"
        }),
        __metadata("design:type", Date)
    ], ProductBillDetailPpob.prototype, "date_created", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], ProductBillDetailPpob.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], ProductBillDetailPpob.prototype, "updated_at", void 0);
    ProductBillDetailPpob = __decorate([
        typeorm_1.Entity({ name: "product_bill_ppob_details" })
    ], ProductBillDetailPpob);
    return ProductBillDetailPpob;
})();
exports.ProductBillDetailPpob = ProductBillDetailPpob;
module.exports = {
    ProductBillDetailPpob: ProductBillDetailPpob
};
//# sourceMappingURL=ProductBillDetailPpob.js.map