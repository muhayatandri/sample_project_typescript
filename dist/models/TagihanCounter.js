"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TagihanCounter = void 0;
const typeorm_1 = require("typeorm");
let TagihanCounter = /** @class */ (() => {
    let TagihanCounter = class TagihanCounter {
    };
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], TagihanCounter.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], TagihanCounter.prototype, "counter_tg", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], TagihanCounter.prototype, "counter_tk", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 50,
            nullable: true,
            type: "integer"
        }),
        __metadata("design:type", Object)
    ], TagihanCounter.prototype, "value", void 0);
    __decorate([
        typeorm_1.Column({
            // length: 255,
            nullable: true,
            type: "timestamp"
        }),
        __metadata("design:type", Date)
    ], TagihanCounter.prototype, "date_created", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], TagihanCounter.prototype, "created_at", void 0);
    __decorate([
        typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
        __metadata("design:type", Date)
    ], TagihanCounter.prototype, "updated_at", void 0);
    TagihanCounter = __decorate([
        typeorm_1.Entity({ name: "tagihan_counters" })
    ], TagihanCounter);
    return TagihanCounter;
})();
exports.TagihanCounter = TagihanCounter;
module.exports = {
    TagihanCounter: TagihanCounter
};
//# sourceMappingURL=TagihanCounter.js.map