"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require('express-group-routes');
const bodyParser = __importStar(require("body-parser"));
const GlobalRoute_1 = require("./routes/GlobalRoute");
const compression_1 = __importDefault(require("compression")); // compresses requests
const cors_1 = __importDefault(require("cors"));
const typeorm_1 = require("typeorm");
const morgan_1 = __importDefault(require("morgan"));
const passport_1 = __importDefault(require("passport"));
// import PassportAuth from "./security/PassportAuth";
require('express-group-routes');
var timeout = require('express-timeout-handler');
const { exec } = require('child_process');
const CronJob = require('cron').CronJob;
const restartCommand = "pm2 restart 0";
const listCommand = "pm2 list";
class App {
    constructor() {
        // let ts = new TransactionBot();
        // ts.Connect();
        // ts.SendMessage();
        // ts.GetMessage();
        this.app = express_1.default();
        this.config();
        App.db();
        this.routePrev = new GlobalRoute_1.GlobalRoute(this.app);
    }
    config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(compression_1.default());
        this.app.use(cors_1.default({ credentials: true, origin: true }));
        this.app.use(morgan_1.default('dev'));
        this.app.use(passport_1.default.initialize());
        this.app.use(passport_1.default.session());
        // new PassportAuth(this.app,passport);
    }
    static db() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(new Date("2020-05-31 10:39:26.329000 +00:00").toISOString());
            let startDate = new Date();
            let endDate = new Date();
            startDate.setHours(7, 0o1, 0o0);
            endDate.setHours(30, 59, 59);
            // startDate.setHours(startDate.setHours(21,10,10))
            console.log("end", endDate);
            const filters = {
                before: startDate.toISOString(),
                after: endDate.toISOString(),
            };
            yield typeorm_1.createConnection('development').then((conn) => __awaiter(this, void 0, void 0, function* () {
                console.log("Database Connected is : ", conn.isConnected);
            }));
            // let nm = "1234"
            // const model = getConnection("development").getRepository(Tagihan);
            // let tes = await model.createQueryBuilder("tagihan")
            //     .where("tagihan.nomor_rekening = :nomor_rekening", {nomor_rekening : "123456"})
            //     .orderBy("tagihan.id", "DESC")
            //     .getOne()
            // // @ts-ignore
            // if (tes !=null){
            //     console.log(startDate)
            //     console.log(endDate)
            //     if (tes.created_at >= startDate && tes.created_at <= endDate) {
            //         console.log("today")
            //     } else {
            //         console.log("not today")
            //     }
            // }else {
            //     console.log("cret")
            // }
            // console.log(tes)
            // const model = getConnection("development").getRepository(Jabbers);
            // let data: any[] = await model.find({where: {jabber_id: 1}});
            // console.log(data)
            // let b = new BotTransactionProcess();
            // let promise = b.Connect();
        });
    }
}
exports.default = new App().app;
//# sourceMappingURL=App.js.map