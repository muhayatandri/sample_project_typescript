"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const { v4: uuidv4 } = require('uuid');
const xmpp = require('simple-xmpp');
class TransactionBot {
    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }
    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }
    Connect(jid, password, host, port = 5222) {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
                // jid: 'qubli@jabbim.com',
                // password: 'qubli00',
                // host: 'jabbim.com',
                // port: 5222,
                // tls: true
            });
            this.SetLogin(true);
        }
    }
    statusConnection() {
        // console.log("status login : " + this.validLogin);
        let self = this;
        try {
            xmpp.on('online', function (d) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                else {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        }
        catch (e) {
            self.SetLogin(false);
            console.log(e);
        }
    }
    SendMessage(message, toJid) {
        xmpp.send(toJid, message, false);
    }
    GetMessage() {
        let self = this;
        try {
            xmpp.on('chat', function (from, message) {
                if (message == null) {
                    console.log("no response");
                }
                // console.log("from other", from);
                self.HandleMessage(message, from);
            });
        }
        catch (e) {
            console.log("something error");
            self.SetLogin(false);
        }
    }
    HandleMessage(message, fromJid) {
        try {
            let obj = {
                uuid: uuidv4(),
                message_responses: message,
                note: "transaksi sedang di proses jabber.",
                is_sent: 1,
                jabber: fromJid,
                date_created: new Date().toISOString()
            };
            let resp = this.TransRepos.SaveParsingProduct(obj);
            if (resp != null || resp != undefined) {
                console.log("Success data insert to parsing product.");
            }
            else {
                console.log("something error");
            }
        }
        catch (e) {
            console.log(e);
            console.log("something error");
        }
    }
    SetLogin(isLogin) {
        this.validLogin = isLogin;
    }
    IsLogin() {
        return this.validLogin;
    }
    CloseConnection() {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }
    ConnectionError() {
        let self = this;
        xmpp.on('error', function (err) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        });
    }
}
exports.TransactionBot = TransactionBot;
//# sourceMappingURL=TransactionBot.js.map