"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnitedProductBot = void 0;
const xmpp = require('simple-xmpp');
class UnitedProductBot {
    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }
    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }
    Connect(jid, password, host, port = 5222) {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
            });
            this.SetLogin(true);
        }
    }
    statusConnection() {
        let self = this;
        try {
            xmpp.on('online', function (d) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                else {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        }
        catch (e) {
            this.validLogin = false;
            console.log(e);
        }
    }
    SendMessage(transCode, toJid) {
        xmpp.send("super_jet@jabberix.com", transCode + ".0300", false);
    }
    GetMessage() {
        try {
            let self = this; // save object reference
            xmpp.on('chat', function (from, message) {
                if (message == null) {
                    console.log("no response");
                }
                // console.log("from other", message);
                self.UnitedRealtimePrice(message);
            });
        }
        catch (e) {
            this.validLogin = false;
            console.log("EXIT : ", e);
        }
    }
    UnitedRealtimePrice(msg) {
        // /Rp
        const regexPrice = /(SP\d*).{3}(Rp.\d+\.\d+)/gm;
        const strPrice = `HARGA PRODUK : TELKOMSEL PROMO 150000 | SP150 = Rp.146.000 | SP100 = Rp.96.625 | SP1 = Rp.1.300 | SP15 = Rp.14.775|SP10 = Rp.10.210 |`;
        let m;
        while ((m = regexPrice.exec(msg)) !== null) {
            if (m.index === regexPrice.lastIndex) {
                regexPrice.lastIndex++;
            }
            let tes = m.reduce((match, groupIndex, index) => {
                if (index === 1) {
                    match.push(groupIndex);
                }
                return match;
            }, []);
            console.log(tes);
        }
        const regexGangguan = /(SP\d*).{3}(Rp.\d+\.\d+).?(\[(.*?)\])/gm;
        const strGangguan = `HARGA PRODUK : TELKOMSEL PROMO 150000 | SP150 = Rp.146.000 | SP100 = Rp.96.625 | SP1 = Rp.1.300 [G] | SP15 = Rp.14.775|SP10 = Rp.10.210 |`;
        // let m;
        while ((m = regexGangguan.exec(msg)) !== null) {
            if (m.index === regexGangguan.lastIndex) {
                regexGangguan.lastIndex++;
            }
            m.forEach((match, groupIndex) => {
                console.log(`Found match, group ${groupIndex}: ${match}`);
            });
        }
    }
    HandleUpdateDatabase() {
    }
    SetLogin(isLogin) {
        this.validLogin = isLogin;
    }
    IsLogin() {
        return this.validLogin;
    }
    CloseConnection() {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }
    ConnectionError() {
        let self = this;
        xmpp.on('error', function (err) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        });
    }
}
exports.UnitedProductBot = UnitedProductBot;
//# sourceMappingURL=UnitedProductBot.js.map