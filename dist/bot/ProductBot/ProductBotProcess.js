"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductBotProcess = void 0;
const xmpp = require('simple-xmpp');
class ProductBotProcess {
    constructor() {
    }
    Connect() {
        this.simpleXmpp = xmpp.connect({
            jid: 'qubli@jabbim.com',
            password: 'qubli00',
            host: 'jabbim.com',
            port: 5222,
            tls: true
        });
    }
    SendMessage() {
        xmpp.send('ctr.01@unichat.in', 'CH.SP1', false);
    }
    GetMessage() {
        xmpp.on('chat', function (from, message) {
            if (message == null) {
                console.log("no response");
            }
            console.log("from other", message);
        });
    }
}
exports.ProductBotProcess = ProductBotProcess;
//# sourceMappingURL=ProductBotProcess.js.map