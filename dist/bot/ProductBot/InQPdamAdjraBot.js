"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InQPdamAdjraBot = void 0;
const xmpp = require('simple-xmpp');
class InQPdamAdjraBot {
    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }
    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }
    Connect(jid, password, host, port = 5222) {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
            });
            this.SetLogin(true);
        }
    }
    statusConnection() {
        let self = this;
        try {
            xmpp.on('online', function (d) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                else {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        }
        catch (e) {
            this.validLogin = false;
            console.log(e);
        }
    }
    SendMessage(message, toJid) {
        xmpp.send("super_jet@jabberix.com", message + ".0300", false);
    }
    GetMessage() {
        xmpp.on('chat', function (from, message) {
            if (message == null) {
                console.log("no response");
            }
            console.log("from other", message);
        });
    }
    SetLogin(isLogin) {
        this.validLogin = isLogin;
    }
    IsLogin() {
        return this.validLogin;
    }
    CloseConnection() {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }
    ConnectionError() {
        let self = this;
        xmpp.on('error', function (err) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        });
    }
}
exports.InQPdamAdjraBot = InQPdamAdjraBot;
//# sourceMappingURL=InQPdamAdjraBot.js.map