"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InqPpobArdraBot = void 0;
const xmpp = require('simple-xmpp');
class InqPpobArdraBot {
    // public TransactionBot(): TransactionBot {
    //     if (this.TransBot != null) {
    //         this.TransBot = new TransactionBot();
    //     }
    //     return this.TransBot
    // }
    constructor() {
        // this.jid = jid;
        // this.password = password;
        // this.toJid = toJid;
        // this.host = host;
        // this.port = port
    }
    Connect(jid, password, host, port = 5222) {
        if (!this.IsLogin()) {
            console.log("Trying to connect");
            xmpp.connect({
                jid: jid,
                password: password,
                host: host,
                port: port,
                tls: true
            });
            this.SetLogin(true);
        }
    }
    statusConnection() {
        let self = this;
        try {
            xmpp.on('online', function (d) {
                if (self.IsLogin()) {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                else {
                    console.log("STATUS LOGIN : ", self.IsLogin());
                }
                console.log('Terkoneksi dengan Jabber ID in client: ' + d.jid);
            });
        }
        catch (e) {
            this.validLogin = false;
            console.log(e);
        }
    }
    SendMessage(message, toJid, ppobType) {
        this.type = ppobType;
        xmpp.send(toJid, message, false);
    }
    GetMessage() {
        let counter = 0;
        let self = this;
        xmpp.on('chat', function (from, message) {
            if (message == null) {
                console.log("no response");
            }
            if (counter == 1) {
                if (self.type == "pdam") {
                    self.PdamResponseDouble(message);
                }
            }
            if (self.type == "pdam") {
                self.PdamResponseSingle(message, counter);
            }
            console.log("from other", message, counter);
            counter++;
        });
    }
    PdamResponseSingle(message, counter) {
        let data = [];
        if (counter == 0) {
            const regexSdhPrnh = /\S*(\w*|\W+).(\w+).+\.(\d+).+sdh.pernah.\w+.\d+:\w+\D*SN\/Ref:.([\D ]+.*(?:\/?|\-.*){5}).+Saldo.?(\d+?\.\d+\.?\d+)/gm;
            const strSdhPrnh = `TRX LANCAR #1 #27148876  CEKPREPAID.01111731145 sdh pernah jam 14:07, status Sukses. SN/Ref: PT BETA GOLDLAND/R2/5500VA. Trx ke-2/hr: CEKPREPAID.2.01111731145.pin. Saldo 24.736DEP ALFA INDO GGN`;
            let m;
            while ((m = regexSdhPrnh.exec(message)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regexSdhPrnh.lastIndex) {
                    regexSdhPrnh.lastIndex++;
                }
                // The result can be accessed through the `m`-variable.
                m.forEach((match, groupIndex) => {
                    data.push(match);
                    console.log(`Found match, group ${groupIndex}: ${match}`);
                });
            }
        }
    }
    PdamResponseDouble(message) {
        let m;
        let data = [];
        const regexSuccess = /R#\D*(\w*|\W+).(\w+).+\.(\d+).+SUKSES.+SN\/Ref:.?([\D ]+.*(?:\/?|\-.*){5}).+Saldo.?(\d+?\.\d+\.?\d+).-.(\d+?\.\d+\.?\d+|\b\d\b).+=.?(\d+?\.\d+\.?\d+).+@(\d+-\d+.?\d+:\d+)/gm;
        const strSuccess = `TRX LANCAR# R#1 Cek PDAM Aetra Kota Jakarta CEKAEJKT.30008997 SUKSES. SN/Ref: INQ KAHAR/1BLN/MEI 2020/PEM0/MET0-0/LL0/DEN0/ADM2500/RP140484. Saldo 1.426 - 0 = 1.426 @28-05 10:42ALFA/INDO OPEN`;
        while ((m = regexSuccess.exec(message)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regexSuccess.lastIndex) {
                regexSuccess.lastIndex++;
            }
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                console.log(`Found match, group ${groupIndex}: ${match}`);
                console.log(match);
                data.push(match);
            });
        }
        const regexGagal = /R#\D+?.(\w+).+\.(\d+).?GAGAL.([\D ]+)Saldo.(\d+?\.\d+\.?\d+).+@(\d+:\d+)/gm;
        const strGagag = `TRX LANCAR# R#1 Cek PDAM Aetra Kota Jakarta CEKAEJKT.300408997 GAGAL. NOMOR YANG ANDA MASUKAN SALAH . Saldo 1.426 @10:57ALFA/INDO OPEN`;
        while ((m = regexGagal.exec(message)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regexGagal.lastIndex) {
                regexGagal.lastIndex++;
            }
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                data.push(match);
                console.log(`Found match, group ${groupIndex}: ${match}`);
            });
        }
        const regexCutOf = /R#\D+?.(\w+).+\.(\d+).?GAGAL.([\D ]+)Saldo.(\d+?\.\d+\.?\d+).+@(\d+:\d+)/gm;
        const str = `TRX LANCAR# R#100 Cek PDAM Temanggung CPTMG.01350439 GAGAL. SEDANG DALAM MASA CUT OFF, SILAHKAN COBA BEBERAPA SAAT KEMUDIAN . Saldo 1.426 @18:08ALFA/INDO OPEN`;
        while ((m = regexCutOf.exec(message)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regexCutOf.lastIndex) {
                regexCutOf.lastIndex++;
            }
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                console.log(`Found match, group ${groupIndex}: ${match}`);
            });
        }
    }
    ResponseApi() {
    }
    SaveDatabase(data) {
    }
    SetInQ(type, nomorRekeninig) {
        this.type = type;
        this.nomorRekening = nomorRekeninig;
    }
    SetLogin(isLogin) {
        this.validLogin = isLogin;
    }
    IsLogin() {
        return this.validLogin;
    }
    CloseConnection() {
        let self = this;
        xmpp.on('close', function () {
            xmpp.conn.end();
            self.SetLogin(false);
            console.log('connection has been closed!');
        });
    }
    ConnectionError() {
        let self = this;
        xmpp.on('error', function (err) {
            console.error("ERR : ", err);
            xmpp.conn.end();
            self.SetLogin(false);
        });
    }
}
exports.InqPpobArdraBot = InqPpobArdraBot;
//# sourceMappingURL=InqPpobArdraBot.js.map