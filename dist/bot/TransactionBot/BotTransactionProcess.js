"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const TransactionPpobRepository_1 = require("../../Repositories/TransactionPpobRepos/TransactionPpobRepository");
const TransactionBot_1 = require("./TransactionBot");
const typeorm_1 = require("typeorm");
class BotTransactionProcess {
    constructor() {
        this.transRepos = new TransactionPpobRepository_1.TransactionPpobRepository();
        this.transBot = new TransactionBot_1.TransactionBot();
        this.transBot.TransRepos = this.transRepos;
        // BotTransactionProcess.db();
        // this.transBot.GetMessage(); //use this
        // this.transBot.CloseConnection();
    }
    Connect() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.transRepos.InitDb();
        });
    }
    DoProcess() {
        return __awaiter(this, void 0, void 0, function* () {
            this.transBot.GetMessage();
            yield this.transRepos.InitDb();
            let self = this;
            let jabber = yield self.transRepos.GetJabberBy({ id: '' });
            if (jabber.is_center_transaction != 0 && jabber.is_gangguan == 0) {
                let jdid = jabber.jabber;
                this.transBot.Connect(jdid, "qubli00", "jabbim.com");
            }
            this.transBot.statusConnection();
            try {
                let data = ["MK5", "MK10", "MK50"];
                let o = 0;
                let interval = setInterval(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        // if (o === 50) {
                        //     clearInterval(interval); //Call the method when you want to clear it.
                        // }
                        // for (let i of data) {
                        //     self.transBot.SendMessage(i + ".08521115032" + o, "super_jet@jabberix.com")
                        // }
                        // console.log(o);
                        o++;
                        if (self.transBot.IsLogin()) {
                            let datas = yield self.transRepos.GetJabberMessage({ jabber_id: '' });
                            if (datas.length > 0) {
                                for (let i of datas) {
                                    let msg = i.messages;
                                    let toJid = i.jabber.jabber;
                                    self.transBot.SendMessage(msg, toJid);
                                    yield self.transRepos.UpdateById({ is_sent: 1 }, i.id);
                                }
                            }
                        }
                        else {
                            console.log("Err! Bot must to login");
                        }
                    });
                }, 2000);
            }
            catch (e) {
                console.log("Err!bot must be restart.");
                this.transBot.CloseConnection();
                this.transBot.ConnectionError();
            }
        });
    }
    ResDoProcess(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.transRepos.InitDb();
            let self = this;
            let jabber = yield self.transRepos.GetJabberBy({ id: '' });
            if (jabber.is_center_transaction != 0 && jabber.is_gangguan == 0) {
                let jdid = jabber.jabber;
                this.transBot.Connect(jdid, "qubli00", "jabbim.com");
            }
            this.transBot.statusConnection();
            try {
                let data = ["MK5", "MK10", "MK50"];
                let o = 0;
                let interval = setInterval(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        // if (o === 50) {
                        //     clearInterval(interval); //Call the method when you want to clear it.
                        // }
                        // for (let i of data) {
                        //     self.transBot.SendMessage(i + ".08521115032" + o, "super_jet@jabberix.com")
                        // }
                        console.log(o);
                        o++;
                        if (self.transBot.IsLogin()) {
                            let datas = yield self.transRepos.GetJabberMessage({ jabber_id: '' });
                            if (datas.length > 0) {
                                for (let i of datas) {
                                    let msg = i.messages;
                                    let toJid = i.jabber.jabber;
                                    self.transBot.SendMessage(msg, toJid);
                                    yield self.transRepos.UpdateById({ is_sent: 1 }, i.id);
                                }
                            }
                        }
                        else {
                            console.log("Err! Bot must to login");
                        }
                    });
                }, 2000);
            }
            catch (e) {
                console.log("Err!bot must be restart.");
                this.transBot.CloseConnection();
                this.transBot.ConnectionError();
            }
        });
    }
    RestartBot(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    db() {
        typeorm_1.createConnection("development").then((conn) => __awaiter(this, void 0, void 0, function* () {
            console.log("Database Connected is : ", conn.isConnected);
        }));
    }
}
exports.BotTransactionProcess = BotTransactionProcess;
// let b = new BotTransactionProcess();
// b.DoProcess();
// b.db();
// b.Connect().then(function (con: any) {
//     // console.log(con)
// });
//# sourceMappingURL=BotTransactionProcess.js.map