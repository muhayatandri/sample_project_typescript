"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InqPpobApi = void 0;
const InqPpobArdraBot_1 = require("./ProductBot/InqPpobArdraBot");
const typeorm_1 = require("typeorm");
const ProductsPpobRepository_1 = require("../Repositories/ProductsPpobRepos/ProductsPpobRepository");
const TagihanRepository_1 = require("../Repositories/TagihanRepos/TagihanRepository");
class InqPpobApi {
    constructor() {
        this.tagihanRepo = new TagihanRepository_1.TagihanRepository();
        this.productPpobRepo = new ProductsPpobRepository_1.ProductsPpobRepository();
        this.inquiryBot = new InqPpobArdraBot_1.InqPpobArdraBot();
        this.inquiryBot.GetMessage();
    }
    DoProcess(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // await this.productPpobRepo.InitDb()
            this.inquiryBot.Connect("qubli@jabbim.com", "qubli00", "jabbim.com");
            this.inquiryBot.statusConnection();
            let message;
            let request = req.body;
            console.log(req.body.nomor_rekening);
            let product = yield this.productPpobRepo.FindProductBillDetailBy(request.product_detail_id);
            let counter = yield this.productPpobRepo.GetCounter();
            this.inquiryBot.SetInQ(request.provider_ppob, request.nomor_rekening);
            let startDate = new Date();
            let endDate = new Date();
            startDate.setHours(7, 0o0, 0o0);
            endDate.setHours(30, 59, 59);
            if (product == null) {
                return res.status(424).json({ "error": true, "status": 424, "message": "Error in database!" });
            }
            let current = yield this.tagihanRepo.FindTagihanBy(request.nomor_rekening);
            if (current != null) {
                // console.log("start", startDate)
                // console.log("end", endDate)
                let currDate = new Date(current.created_at);
                currDate.setTime(currDate.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
                console.log("cc", currDate.toISOString());
                if (currDate >= startDate && current.created_at <= currDate) {
                    message = product.check_code + "." + request.nomor_rekening + ".0300R#" + counter.value;
                    yield this.productPpobRepo.UpdateCounter({ value: counter.value + 1 }, counter.id);
                    console.log("today");
                }
                else {
                    yield this.productPpobRepo.UpdateCounter({ value: 2 }, counter.id);
                    message = product.check_code + "." + request.nomor_rekening + ".0300R#1";
                    console.log("not today");
                }
            }
            else {
                message = product.check_code + "." + request.nomor_rekening + ".0300R#1";
                yield this.tagihanRepo.CreateReqCheckTagihan({
                    "phone_number": "",
                    "nomor_rekening": request.nomor_rekening,
                    "product_name": product.name,
                    "status": "Proses check",
                    // "user_id": null,
                    "date_created": Date.now()
                });
            }
            console.log(message);
            // check whether customer request again to check bill or no,
            // message = "CEKAEJKT.30008997 + \".0300R#\" + counter"
            this.inquiryBot.SendMessage(message, "super_jet@jabberix.com", request.provider_ppob);
            return res.status(200).json("");
        });
    }
    db() {
        typeorm_1.createConnection("development").then((conn) => __awaiter(this, void 0, void 0, function* () {
            console.log("Database Connected is : ", conn.isConnected);
        }));
    }
}
exports.InqPpobApi = InqPpobApi;
//# sourceMappingURL=InqPpobApi.js.map