"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const TransactionPpobs_1 = require("../models/TransactionPpobs");
class TransactionPpobRepository {
    static getConnectionUser() {
        return typeorm_1.getConnection("development").getRepository(TransactionPpobs_1.TransactionPpobs);
    }
}
//# sourceMappingURL=TransactionPpobRepository.js.map