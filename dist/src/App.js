"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require('express-group-routes');
const bodyParser = __importStar(require("body-parser"));
const GlobalRoute_1 = require("./routes/GlobalRoute");
const compression_1 = __importDefault(require("compression")); // compresses requests
const cors_1 = __importDefault(require("cors"));
const typeorm_1 = require("typeorm");
const morgan_1 = __importDefault(require("morgan"));
const passport_1 = __importDefault(require("passport"));
// import PassportAuth from "./security/PassportAuth";
require('express-group-routes');
class App {
    constructor() {
        // let ts = new TransactionBot();
        // ts.Connect();
        // ts.SendMessage();
        // ts.GetMessage();
        this.app = express_1.default();
        this.config();
        App.db();
        this.routePrev = new GlobalRoute_1.GlobalRoute(this.app);
    }
    config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(compression_1.default());
        this.app.use(cors_1.default({ credentials: true, origin: true }));
        this.app.use(morgan_1.default('dev'));
        this.app.use(passport_1.default.initialize());
        this.app.use(passport_1.default.session());
        // new PassportAuth(this.app,passport);
    }
    static db() {
        return __awaiter(this, void 0, void 0, function* () {
            yield typeorm_1.createConnection('development').then((conn) => __awaiter(this, void 0, void 0, function* () {
                console.log("Database Connected is : ", conn.driver);
            }));
            // let b = new BotTransactionProcess();
            // let promise = b.Connect();
        });
    }
}
exports.default = new App().app;
//# sourceMappingURL=App.js.map