"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class RequestNet {
    constructor(data, url) {
        this.data = data;
        this.url = url;
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield fetch(this.url);
            const data = res.json();
            return new ParsedResponse("", data).response; // passed the response to another class
        });
    }
    post() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    put() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.RequestNet = RequestNet;
class ParsedResponse {
    constructor(url, result) {
        this._response = result;
        this.url = url;
    }
    get response() {
        return this._response;
    }
}
exports.ParsedResponse = ParsedResponse;
//# sourceMappingURL=RequestNet.js.map