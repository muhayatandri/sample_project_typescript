"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const TransactionPpobRepository_1 = require("../Repositories/TransactionPpobRepos/TransactionPpobRepository");
const TransactionBot_1 = require("./TransactionBot");
const typeorm_1 = require("typeorm");
class BotTransactionProcess {
    constructor() {
        this.transRepos = new TransactionPpobRepository_1.TransactionPpobRepository();
        this.transBot = new TransactionBot_1.TransactionBot();
        this.transBot.TransRepos = this.transRepos;
        // BotTransactionProcess.db();
        // this.transBot.GetMessage();
        // this.transBot.CloseConnection();
    }
    Connect() {
        return __awaiter(this, void 0, void 0, function* () {
            let data = yield this.transRepos.GetJabberMessage();
            if (data.length > 0) {
                console.log(data);
            }
            else {
                console.log("tes");
            }
        });
    }
    DoProcess() {
        let self = this;
        this.transBot.Connect("qubli@jabbim.com", "qubli00", "jabbim.com");
        this.transBot.statusConnection();
        // self.transBot.SendMessage("MK5.085211150323", "super_jet@jabberix.com");
        try {
            let data = ["MK5", "MK10", "MK50"];
            let o = 0;
            let interval = setInterval(function () {
                return __awaiter(this, void 0, void 0, function* () {
                    if (o === 10) {
                        clearInterval(interval); //Call the method when you want to clear it.
                    }
                    for (let i of data) {
                        self.transBot.SendMessage(i + ".08521115032" + o, "super_jet@jabberix.com");
                    }
                    o++;
                    // console.log("hi");
                    // await self.transRepos.UpdateById({is_sent: 1}, 1)
                });
            }, 1000);
        }
        catch (e) {
            this.transBot.CloseConnection();
            this.transBot.ConnectionError();
        }
    }
    static db() {
        return __awaiter(this, void 0, void 0, function* () {
            yield typeorm_1.createConnection('development').then((conn) => __awaiter(this, void 0, void 0, function* () {
                console.log("Database Connected is : ", conn.isConnected);
            }));
            // let b = new BotTransactionProcess();
            // let promise = b.Connect();
        });
    }
}
exports.BotTransactionProcess = BotTransactionProcess;
let b = new BotTransactionProcess();
b.Connect();
//# sourceMappingURL=BotTransactionProcess.js.map