"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let JabberSent = class JabberSent {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], JabberSent.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        // length: 50,
        nullable: true,
        type: "integer"
    }),
    __metadata("design:type", Object)
], JabberSent.prototype, "is_sent", void 0);
__decorate([
    typeorm_1.Column({
        // length: 50,
        nullable: true,
        type: "integer"
    }),
    __metadata("design:type", Object)
], JabberSent.prototype, "trans_ppob_id", void 0);
__decorate([
    typeorm_1.Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    }),
    __metadata("design:type", String)
], JabberSent.prototype, "to_jid", void 0);
__decorate([
    typeorm_1.Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    }),
    __metadata("design:type", String)
], JabberSent.prototype, "types", void 0);
__decorate([
    typeorm_1.Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    }),
    __metadata("design:type", String)
], JabberSent.prototype, "messages", void 0);
__decorate([
    typeorm_1.Column({
        // length: 50,
        nullable: true,
        type: "character varying"
    }),
    __metadata("design:type", String)
], JabberSent.prototype, "JabberId", void 0);
__decorate([
    typeorm_1.Column({
        // length: 255,
        nullable: true,
        type: "timestamp"
    }),
    __metadata("design:type", Date)
], JabberSent.prototype, "created_date", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
    __metadata("design:type", Date)
], JabberSent.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', }),
    __metadata("design:type", Date)
], JabberSent.prototype, "updated_at", void 0);
JabberSent = __decorate([
    typeorm_1.Entity()
], JabberSent);
exports.JabberSent = JabberSent;
module.exports = {
    JabberSent: JabberSent
};
//# sourceMappingURL=JabberSent.js.map