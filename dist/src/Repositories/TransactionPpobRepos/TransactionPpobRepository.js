"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const TransactionPpobs_1 = require("../../models/TransactionPpobs");
const JabberSent_1 = require("../../models/JabberSent");
class TransactionPpobRepository {
    static getConnectionUser() {
        return typeorm_1.getConnection("development").getRepository(TransactionPpobs_1.TransactionPpobs);
    }
    static getConnectionJabber() {
        return typeorm_1.getConnection("development").getRepository(JabberSent_1.JabberSent);
    }
    static getConnectionJabber2() {
        return typeorm_1.getRepository(JabberSent_1.JabberSent);
    }
    GetTransactionBy(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = TransactionPpobRepository.getConnectionUser();
            return yield model.find();
        });
    }
    GetJabberMessage() {
        return __awaiter(this, void 0, void 0, function* () {
            const model = TransactionPpobRepository.getConnectionJabber2();
            return yield model.find({ where: { is_sent: 0 } });
        });
    }
    UpdateById(obj, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = TransactionPpobRepository.getConnectionJabber();
            return yield model.createQueryBuilder()
                .update()
                .set(obj)
                .where("id =:id", { id: id })
                // .update({...obj})
                .returning('*')
                .execute();
        });
    }
}
exports.TransactionPpobRepository = TransactionPpobRepository;
//# sourceMappingURL=TransactionPpobRepository.js.map